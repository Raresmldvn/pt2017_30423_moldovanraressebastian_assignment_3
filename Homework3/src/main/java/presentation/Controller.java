package presentation;

import java.awt.event.ActionEvent;
import java.io.IOException;

import javax.swing.*;
import businessLayer.*;
import dataAccessLayer.*;
import java.awt.event.*;
import javax.swing.*;
import model.*;
import java.util.*;

/**
 * This class deals with taking events and acting correspondingly for every button.
 * It uses 14 listeners for the 14 buttons and the constructor adds them to the interface.
 * @author rares
 * @version 1.0
 * @since 2017-04-15
 */
public class Controller {

	private View view;
	private CustomerBLL cBLL;
	private ProductBLL pBLL;
	private OrderBLL oBLL;
	private OrderDetailsBLL odBLL;
	private BillGenerator bGenerator;
	
	public Controller(View oneView) {
		
		this.view = oneView;
		this.cBLL = new CustomerBLL();
		this.pBLL = new ProductBLL();
		this.oBLL = new OrderBLL();
		this.odBLL = new OrderDetailsBLL();
		this.bGenerator = new BillGenerator();
		view.addCustomerDeleteListener(new DeleteCustomerListener());
		view.addProductDeleteListener(new DeleteProductListener());
		view.addCustomerUpdateListener(new UpdateCustomerListener());
		view.addProductUpdateListener(new UpdateProductListener());
		view.addCustomerAddListener(new AddCustomerListener());
		view.addFinalizeCustomerListener(new FinalizeAddCustomerListener());
		view.addProductAddListener(new AddProductListener());
		view.addFinalizeProductistener(new FinalizeAddProductListener());
		view.addNewOrderListener(new NewOrderListener());
		view.addAddToOrderListener(new AddToOrderListener());
		view.addFinalizeOrderListener(new FinalizeOrderListener());
		view.addDropOrderListener(new DropOrderListener());
		view.addOpenCustomerListener(new OpenCustomerListener());
		view.addOpenProductListener(new OpenProductListener());
	}
	
	public class DeleteCustomerListener implements ActionListener {
		
		public void actionPerformed(ActionEvent e){
			
			JTable customers = view.getCustomerTable();
			cBLL.deleteFromTable(customers); //delete customers
			customers = cBLL.createTable(new CustomerDAO().selectAll()); //create new table
			view.repaintComponents(customers, view.getProductTable()); //repaint table 
		}
		}
	
	
	public class OpenCustomerListener implements ActionListener {
		
		public void actionPerformed(ActionEvent e) {
			
			view.setCustomerVisible();
		}
	}
	
	public class OpenProductListener implements ActionListener {
		
		public void actionPerformed(ActionEvent e) {
			
			view.setProductVisible();
		}
	}
	
	
public class DeleteProductListener implements ActionListener {
		
		public void actionPerformed(ActionEvent e){
			
			JTable products = view.getProductTable();
			pBLL.deleteFromTable(products);
			products = pBLL.createTable(new ProductDAO().selectAll());
			view.repaintComponents(view.getCustomerTable(), products);
		}
		}

public class UpdateCustomerListener implements ActionListener {
	
		public void actionPerformed(ActionEvent e) {
			
			JTable customers = view.getCustomerTable(); //get the current customer table
			cBLL.updateFromTable(customers);
			customers = cBLL.createTable(new CustomerDAO().selectAll()); //create new table
			view.repaintComponents(customers, view.getProductTable()); //repaint tables
		}
}

public class UpdateProductListener implements ActionListener {
	
	public void actionPerformed(ActionEvent e) {
		JTable products = view.getProductTable();
		pBLL.updateFromTable(products);
		products = pBLL.createTable(new ProductDAO().selectAll());
		view.repaintComponents(view.getCustomerTable(), products);
	}
}

public class AddCustomerListener implements ActionListener {
	
	public void actionPerformed(ActionEvent e) {
		
		view.setAddClientFrameVisible();
	}
}

public class FinalizeAddCustomerListener implements ActionListener {
	
	public void actionPerformed(ActionEvent e) {
		
		//find next available index
		List<Customer> customers = new CustomerDAO().selectAll();
		int id;
		if(customers.size()==0){
			id=1;
		} else{
		id = customers.get(customers.size()-1).getId()+1;
		}
		//get values from fields and insert
		Customer newCustomer = new Customer(id, view.getCustomerName(), view.getCustomerAddress(), view.getCustomerEmail(), view.getCustomerPhone());
		try {
			cBLL.insertCustomer(newCustomer);
		} catch(IOException exc) {
			
			view.showError(exc.getMessage());
		}
		
		//repaint table
		JTable customersTable = cBLL.createTable(new CustomerDAO().selectAll());
		view.repaintComponents(customersTable, view.getProductTable());
	}
}

public class AddProductListener implements ActionListener{
	
	public void actionPerformed(ActionEvent e) {
		
		view.setAddProductFrameVisible();
	}
}

public class FinalizeAddProductListener implements ActionListener {
	
	public void actionPerformed(ActionEvent e) {
		
		List<Product> products = new ProductDAO().selectAll(); //select all products
		int id; //find the id by taking the last id from the list of objects and incremening it
		if(products.size()==0)
			id=1;
		else
		    id = products.get(products.size()-1).getId() +1;
		
		Product newProduct = new Product(id, view.getProductName(), view.getProductStock(), view.getProductPrice());
		try {
			
			pBLL.insertProduct(newProduct); //insert the new product
		} catch(Exception exc) {
			
			view.showError(exc.getMessage());
		}
		
		JTable productsTable = pBLL.createTable(new ProductDAO().selectAll());
		view.repaintComponents(view.getCustomerTable(), productsTable); //repaint product table
	}
}

public class NewOrderListener implements ActionListener {
	
	public void actionPerformed(ActionEvent e) {
		
		try{
		int id; 
		Customer orderingCustomer = cBLL.getSelected(view.getCustomerTable());
		view.setCustomerLabel(orderingCustomer.getName());
		view.setOrderFrameVisible();
		List<Orders> all = (new OrderDAO()).selectAll();
		if(all.size() == 0)  //create id  by selecting the last index of the last element in the list and incremeting it 
			id = 1;
		else
			id= all.get(all.size()-1).getId() +1;
		oBLL.insertOrder(new Orders(id, orderingCustomer.getId(), 0));
		}catch(Exception exc) {
			
			view.showError("Select a customer!" + exc.getMessage());
		}
	}
}

public class AddToOrderListener implements ActionListener {
	
	public void actionPerformed(ActionEvent e) {
		
		JTable productTable = view.getProductTable();
		Product selected = pBLL.getSelected(productTable);
		if(view.getQuantityField()>selected.getStock()){
			
			view.showError("UNDERSTOCK!"); //show understock error when required quantity is grater than the stock
		}
		else{
			int quantity = view.getQuantityField();
			Orders currentOrder = oBLL.getOrder();
			System.out.println(currentOrder.getId());
			List<OrderDetails> det = new OrderDetailsDAO().selectAll();
			
			int id;
			if(det.size()==0) {
				id=1;
			}
			else
				id = det.get(det.size()-1).getId() + 1;
			OrderDetails newOD = new OrderDetails(id, currentOrder.getId(), selected.getId(), quantity); //create new order details
			odBLL.insert(newOD); //insert the order details in the database
			view.appendTextArea(selected.toString() + " x " + quantity); //apend to the "real-time bill"
			oBLL.updateOrder(new Orders(currentOrder.getId(), currentOrder.getCustomerid(), currentOrder.getTotal() + quantity*selected.getPrice()), currentOrder.getId());
			view.setTotal("Total: " + Float.toString(currentOrder.getTotal() + quantity*selected.getPrice())); //set the total label as the sum of the previous total and the new amount
		}
	}
}

	public class FinalizeOrderListener implements ActionListener {
		
		public void actionPerformed(ActionEvent e) {
			
			bGenerator.generatePDFBill(oBLL.getOrder(), odBLL.getList());
			oBLL.emptyCurrentOrder();
			odBLL.updateStocks();
			view.clearTextArea();
			view.clearQuantityField();
			JTable productsTable = pBLL.createTable(new ProductDAO().selectAll());
			view.repaintComponents(view.getCustomerTable(), productsTable);
			view.setTotal("Total");
		}
	}
	
	public class DropOrderListener implements ActionListener {
		
		public void actionPerformed(ActionEvent e) {
			
			oBLL.drop();
			odBLL.drop();
			view.clearTextArea();
			view.clearQuantityField();
			view.setTotal("Total");
		}
	}
}


