package presentation;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.*;
import businessLayer.*;
import dataAccessLayer.*;
import java.awt.event.*;

/**
 * Class that implements the graphical user interface. It consists of 6 main frames: the menu frame, the product editor, the customer editor and the corresponding forms.
 * @author rares
 * @version 1.0
 * @since 2017-04-13
 */
public class View extends JFrame {

	
	private JButton customerEditor = new JButton("Open Customer Editor");
	private JButton productEditor = new JButton("Open Product Editor");
	
	private JTable customerTable;
	private JTable productTable;
	private JPanel tablePanel1 = new JPanel();
	private JPanel tablePanel2 = new JPanel();
	private JButton addNewClient = new JButton("Add Client");
	private JButton addNewProduct = new JButton("Add Product");
	private JButton deleteProduct = new JButton("Delete Product");
	private JButton deleteClient = new JButton("Delete Client");
	private JButton updateClient = new JButton("Update Clients");
	private JButton updateProduct = new JButton("Update Products");
	private JButton createOrder = new JButton("Create Order");
	
	private JFrame frame1; //customer operations
	private JFrame frame2; //product operations
	
	private JFrame frame3; //customer form
	private JTextField cNameField = new JTextField(20);
	private JTextField cAddressField = new JTextField(20);
	private JTextField cEmailField = new JTextField(20);
	private JTextField cPhoneField = new JTextField(20);
	private JButton finalizeAddClient = new JButton("Finalize");
	
	private JFrame frame4; //product form
	private JTextField pNameField = new JTextField(20);
	private JTextField pStockField = new JTextField(20);
	private JTextField pPriceField = new JTextField(20);
	private JButton finalizeAddProduct = new JButton("Finalize");
	
	private JFrame frame5; //order form
	private JLabel customerLabel = new JLabel();
	private JTextArea orderTrack = new JTextArea(10, 40);
	private JTextField quantityField = new JTextField(20);
	private JButton productInOrder = new JButton("Add product");
	private JButton finalizeOrder = new JButton("Finalize");
	private JButton dropOrder = new JButton("Drop");
	private JLabel total = new JLabel("Total");

	public View() {
		
		
		this.setTitle("Warehouse Administration System");
		this.setLayout(new GridLayout(1,3));
		this.setSize(600, 220);
		JLabel titleLabel = new JLabel("Warehouse Administration System");
		titleLabel.setFont(new Font("Serif", Font.PLAIN, 40));
		customerEditor.setFont(new Font("Serif", Font.PLAIN, 25));
		customerEditor.setBackground(new Color(255,255,0));
		productEditor.setFont(new Font("Serif", Font.PLAIN, 25));
		productEditor.setBackground(new Color(255,0,255));
		JPanel firstPanel = new JPanel();
		firstPanel.add(titleLabel);
		firstPanel.add(customerEditor);
		firstPanel.add(productEditor);
		this.add(firstPanel);
		
		customerTable = (new CustomerBLL()).createTable(new CustomerDAO().selectAll());
		JScrollPane customerSCR = new JScrollPane(customerTable);
		customerSCR.setPreferredSize(new Dimension(700, 600));
		customerSCR.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		
		productTable = (new ProductBLL()).createTable(new ProductDAO().selectAll());
		JScrollPane productSCR = new JScrollPane(productTable);
		productSCR.setPreferredSize(new Dimension(700,500));
		productSCR.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		
		frame1 = new JFrame();
		frame1.setTitle("Customers");
		frame1.setLayout(new GridLayout(3,3));
		addNewClient.setFont(new Font("Serif", Font.PLAIN, 25));
		deleteClient.setFont(new Font("Serif", Font.PLAIN, 25));
		updateClient.setFont(new Font("Serif", Font.PLAIN, 25));
		createOrder.setSize(500, 200);
		createOrder.setFont(new Font("Serif", Font.ITALIC, 40));
		createOrder.setBackground(new Color(255,255,0));
		JPanel buttonPanel = new JPanel();
		buttonPanel.add(addNewClient);
		buttonPanel.add(deleteClient);
		buttonPanel.add(updateClient);
		buttonPanel.setMaximumSize(new Dimension(800,200));
		JPanel orderPanel = new JPanel();
		orderPanel.add(createOrder);
		orderPanel.setMaximumSize(new Dimension(800,100));
		frame1.setSize(800, 350);
		tablePanel1.setLayout(new BorderLayout());
		tablePanel1.add(customerSCR);
		tablePanel1.setPreferredSize(new Dimension(800, 500));
		frame1.add(tablePanel1);
		frame1.add(buttonPanel);
		frame1.add(orderPanel);
		
		frame3 = new JFrame();
		frame3.setSize(300, 200);
		JPanel addPanel = new JPanel();
		addPanel.add(new JLabel("Name"));
		addPanel.add(cNameField);
		addPanel.add(new JLabel("Address"));
		addPanel.add(cAddressField);
		addPanel.add(new JLabel("Email"));
		addPanel.add(cEmailField);
		addPanel.add(new JLabel("Phone number"));
		addPanel.add(cPhoneField);
		addPanel.add(finalizeAddClient);
		frame3.setTitle("Customer Form");
		frame3.add(addPanel);
		
		frame2 = new JFrame();
		frame2.setTitle("Products");
		frame2.setLayout(new GridLayout(2, 2));
		tablePanel2.setLayout(new BorderLayout());
		addNewProduct.setFont(new Font("Serif", Font.PLAIN, 25));
		deleteProduct.setFont(new Font("Serif", Font.PLAIN, 25));
		updateProduct.setFont(new Font("Serif", Font.PLAIN, 25));
		JPanel buttonPanel2 = new JPanel();
		buttonPanel.setSize(new Dimension(800,100));
		buttonPanel2.add(addNewProduct);
		buttonPanel2.add(deleteProduct);
		buttonPanel2.add(updateProduct);
		frame2.setSize(800, 350);
		tablePanel2.add(new JScrollPane(productSCR));
		frame2.add(tablePanel2);
		frame2.add(buttonPanel2);
		
		frame4 = new JFrame();
		frame4.setSize(300, 200);
		JPanel addPanel2 = new JPanel();
		addPanel2.add(new JLabel("Name"));
		addPanel2.add(pNameField);
		addPanel2.add(new JLabel("Stock"));
		addPanel2.add(pStockField);
		addPanel2.add(new JLabel("Price"));
		addPanel2.add(pPriceField);
		addPanel2.add(finalizeAddProduct);
		frame4.setTitle("Product Form");
		frame4.add(addPanel2);
		
		frame5 = new JFrame();
		frame5.setTitle("Order");
		frame5.setSize(500, 400);
		frame5.setLayout(new FlowLayout());
		JPanel titlePanel = new JPanel();
		titlePanel.add(customerLabel);
		customerLabel.setFont(new Font("Serif", Font.PLAIN, 25));
		customerLabel.setText("Prenume Nume       ");
		JPanel textPanel = new JPanel();
		textPanel.add(orderTrack);
		//textPanel.setLayout(new BorderLayout());
		JPanel quantityPanel =  new JPanel();
		quantityPanel.add(new JLabel("Quantity"));
		quantityPanel.add(quantityField);
		quantityPanel.add(total);
		quantityPanel.setLayout(new FlowLayout());
		JPanel orderButtonPanel = new JPanel();
		productInOrder.setFont(new Font("Serif", Font.PLAIN, 25));
		finalizeOrder.setFont(new Font("Serif", Font.PLAIN, 25));
		dropOrder.setFont(new Font("Serif", Font.PLAIN, 25));
		orderButtonPanel.add(productInOrder);
		orderButtonPanel.add(finalizeOrder);
		orderButtonPanel.add(dropOrder);
		frame5.add(titlePanel);
		frame5.add(textPanel);
		frame5.add(quantityPanel);
		frame5.add(orderButtonPanel);
		orderTrack.setEditable(false);
	
		this.setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);

	}
	
	void showError(String errMessage) {
		
		JOptionPane.showMessageDialog(this, errMessage); //display a message dialog that states that an error happened 
	}
	

	public JTable getCustomerTable() {
		
		return customerTable;
	}
	
	public void setCustomerTable(JTable newTable) {
		
		this.customerTable = newTable;
	}
	
	public JTable getProductTable() {
		
		return productTable;
	}
	
	public void setProductTable(JTable newTable) {
		
		this.productTable = newTable;
	}
	
	public void setProductVisible() {
		
		frame2.setVisible(true);
	}
	
	public void setCustomerVisible() {
		
		frame1.setVisible(true);
	}
	
	//Listeners
	
	public void addOpenCustomerListener(ActionListener buttonPressed) {
		
		customerEditor.addActionListener(buttonPressed);
	}
	
	public void addOpenProductListener(ActionListener buttonPressed) {
		
		productEditor.addActionListener(buttonPressed);
	}

	public void addCustomerDeleteListener(ActionListener buttonPressed) {
		
		deleteClient.addActionListener(buttonPressed);
	}
	
	public void addProductDeleteListener(ActionListener buttonPressed) {
		
		deleteProduct.addActionListener(buttonPressed);
	}
	
	public void addCustomerUpdateListener(ActionListener buttonPressed) {
		
		updateClient.addActionListener(buttonPressed);
	}
	
public void addProductUpdateListener(ActionListener buttonPressed) {
		
		updateProduct.addActionListener(buttonPressed);
	}

public void addCustomerAddListener(ActionListener buttonPressed) {
	
	addNewClient.addActionListener(buttonPressed);
}

public void addFinalizeCustomerListener(ActionListener buttonPressed) {
	
	finalizeAddClient.addActionListener(buttonPressed);
}

public void addProductAddListener(ActionListener buttonPressed) {
	
	addNewProduct.addActionListener(buttonPressed);
}

public void addFinalizeProductistener(ActionListener buttonPressed) {
	
	finalizeAddProduct.addActionListener(buttonPressed);
}
	
public void addNewOrderListener(ActionListener buttonPressed) {
	
	createOrder.addActionListener(buttonPressed);
}

public void addAddToOrderListener(ActionListener buttonPressed) {
	
	productInOrder.addActionListener(buttonPressed);
}
	
public void addFinalizeOrderListener(ActionListener buttonPressed) {
	
	finalizeOrder.addActionListener(buttonPressed);
}

public void addDropOrderListener(ActionListener buttonPressed) {
	
	dropOrder.addActionListener(buttonPressed);
}

	public void repaintComponents(JTable newCustomer, JTable newProduct) {
		
		this.customerTable = newCustomer;
		JScrollPane customerSCR = new JScrollPane(customerTable);
		customerSCR.setPreferredSize(new Dimension(700, 600));
		this.productTable = newProduct;
		JScrollPane productSCR= new JScrollPane(productTable);
		productSCR.setPreferredSize(new Dimension(700, 600));
		tablePanel2.removeAll();
		tablePanel2.revalidate();
		tablePanel2.add(productSCR);
		frame2.revalidate();
		tablePanel1.removeAll();
		tablePanel1.revalidate();
		tablePanel1.add(customerSCR);
		frame1.revalidate();
	}
	
	//Customer Form
	public void setAddClientFrameVisible() {
		
		frame3.setVisible(true);
	}
	
	public void setAddClientFrameInvisible() {
		
		frame3.setVisible(false);
	}
	
	public String getCustomerName() {
		
		return cNameField.getText();
	}
	
	public String getCustomerAddress() {
		
		return cAddressField.getText();
	}
	
	public String getCustomerEmail() {
		
		return cEmailField.getText();
	}
	
	public String getCustomerPhone() {
		
		return cPhoneField.getText();
	}
	
	//ProductForm
	public void setAddProductFrameVisible() {
		
		frame4.setVisible(true);
	}
	
	public String getProductName() {
		
		return pNameField.getText();
	}
	
	public int getProductStock() {
		
		return Integer.parseInt(pStockField.getText());
	}
	
	public float getProductPrice() {
		
		return Float.parseFloat(pPriceField.getText());
	}
	
	//Order Form
	
	public void setOrderFrameVisible() {
		
		frame5.setVisible(true);
	}
	public void setCustomerLabel(String name) {
		
		customerLabel.setText(name);
	}
	
	public void appendTextArea(String order) {
		
		orderTrack.append(order + "\n");
	}
	
	public int getQuantityField() {
		
		return Integer.parseInt(quantityField.getText());
	}
	
	public void setTotal(String t) {
		
		total.setText(t);
	}
	public void clearTextArea() {
		
		orderTrack.setText("");
	}
	
	public void clearQuantityField() {
		
		quantityField.setText("");
	}
}
