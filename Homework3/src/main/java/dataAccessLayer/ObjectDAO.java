package dataAccessLayer;

import java.beans.PropertyDescriptor;
import java.lang.reflect.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import connection.ConnectionFactory;
import java.util.*;

/**
 * A parameter-based class which uses the connection with the database to get data regardless of the type of object(by using reflection techniques).
 * @author rares
 * @version 1.0
 * @since 2017-04-14
 * @param <T>
 */
public class ObjectDAO<T> {

	private final Class<T> type;
	
	
	/**
	 * Setse the type parameter accordingly by obtaining information about the class.
	 */
	@SuppressWarnings("unchecked")
	public ObjectDAO() {
		
		ParameterizedType genericSuperclass = (ParameterizedType) this.getClass().getGenericSuperclass();
		Type theType = genericSuperclass.getActualTypeArguments()[0];
		if(theType instanceof Class) {
			
			this.type = (Class<T>) theType; 
		} 
		else {	
		this.type = (Class<T>) ((ParameterizedType)theType).getRawType();
		}
	}
	
	/**
	 * Creates a generic select query using a criteria.
	 * @param criteria can be any field of the database tables
	 * @return Returns the string containing a select query.
	 */
	private String createSelectQuery(String criteria) {
		
		String selectQuery = new String("");
		selectQuery = selectQuery + "SELECT * FROM ";
		selectQuery = selectQuery + type.getSimpleName().toLowerCase();
		selectQuery = selectQuery + " WHERE " + criteria + "=?";
		System.out.println(selectQuery);
		return selectQuery;
	}
	
	/**
	 * Creates a delete query using a criteria.
	 * @param criteria It can be any field of the database tables.
	 * @return Returns the string containing the delete query.
	 */
private String createDeleteQuery(String criteria) {
		
		String selectQuery = new String("");
		selectQuery = selectQuery + "DELETE FROM ";
		selectQuery = selectQuery + type.getSimpleName().toLowerCase();
		selectQuery = selectQuery + " WHERE " + criteria + "=?";
		return selectQuery;
	}
	
	/**
	 * Creates the insert query by using reflection techniques.
	 * @param criteria 
	 * @param object The object to be inserted.
	 * @return Returns the string containing the insertion query.
	 */
	private String createInsertQuery(String criteria, T object) {
		
		Field[] myFields = type.getDeclaredFields();
		String insertQuery = new String("");
		insertQuery = insertQuery + "INSERT INTO ";
		insertQuery = insertQuery + type.getSimpleName().toLowerCase() + " (";
		for(int i=0; i<myFields.length; i++) {
			insertQuery = insertQuery + myFields[i].getName().toLowerCase() + ",";
		}
		insertQuery = insertQuery.substring(0, insertQuery.length()-1);
		insertQuery = insertQuery + ")" + " VALUES (";
		try{
		for(Field currentField: myFields) {
			
			if(currentField.getName().substring(0, 1) !="id") {
				currentField.setAccessible(true);
				Object value = currentField.get(object);
				if(value.toString().matches(".*[a-zA-Z]+.*")==true){
					
					insertQuery = insertQuery + "'" + value.toString() + "'" + ",";
				}
				else {
				insertQuery = insertQuery + value.toString() + ",";
				}
			}
		}
		}catch(Exception e) {}
		insertQuery = insertQuery.substring(0, insertQuery.length()-1) + ")";
		return insertQuery;
	}
	
	/**
	 * Creates the update Query using reflection techniques for taking the fields.
	 * @param criteria The parameter sets the criteria for selecting the updated object
	 * @param updatedObject
	 * @return Returns the query string for updating.
	 */
	private String createUpdateQuery(String criteria, T updatedObject){
		
		Field[] myFields = type.getDeclaredFields();
		String updateQuery = new String("");
		updateQuery = updateQuery + "UPDATE  ";
		updateQuery = updateQuery + type.getSimpleName().toLowerCase() + " SET " ;
		
		try {
		for(Field currentField: myFields) {
			
			if(currentField.getName().substring(0, 2) .compareTo("id")!=0) {
				
				currentField.setAccessible(true);
				updateQuery = updateQuery + currentField.getName() + "=";
				Object value = currentField.get(updatedObject);
				if(value.toString().matches(".*[a-zA-Z]+.*")==true){
					
					updateQuery = updateQuery + "'" + value.toString() + "'" + ", ";
				}
				else {
					
					updateQuery = updateQuery + value.toString() + ", ";
				}
			}
		}
		
		updateQuery = updateQuery.substring(0, updateQuery.length()-2) + " WHERE " + criteria + "=?";
		}catch(Exception e) {
			
			System.out.println(e.getMessage());
		}
		return updateQuery;
	}
	
	/**
	 * Creates a general query for selecting all elements of the database.
	 * @return Returns the select all query. 
	 */
	private String createSelectAllQuery() {
		
		String allQuery = new String("SELECT * FROM ");
		allQuery = allQuery + type.getSimpleName().toLowerCase();
		return allQuery;
	}
	
	/**
	 * This method creates the connection with the database and executes the select query formerly defined.
	 * @param id An id for the object to be selected.
	 * @return An object of the desired type returned by the query.
	 */
	public T selectById(int id) {
		
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet resSet = null;
		String selectQuery = createSelectQuery("id");
		
		T resultingObject = null;
		
		try {
			
			findStatement = connection.prepareStatement(selectQuery);
			findStatement.setInt(1, id);
			resSet = findStatement.executeQuery();
			resultingObject = createObjectList(resSet).get(0);
		} catch(Exception e) {
			
			System.out.println(e.getClass().getName() + e.getMessage());
		}
		
		return resultingObject;
	}
	
	/**
	 * This method executes the selectAll query.
	 * @return Returns a list of elements of the desired type.
	 */
	public List<T> selectAll() {
		
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet resSet = null;
		String selectQuery = createSelectAllQuery();
		
		List<T> resultingObjects = null;
		
		try {
			
			findStatement = connection.prepareStatement(selectQuery);
			resSet = findStatement.executeQuery();
			resultingObjects = createObjectList(resSet);
		} catch(Exception e) {
			
			System.out.println("Here" + e.getClass().getName() + e.getMessage());
		}
		
		return resultingObjects;
	}
	
	/**
	 * The method executes the query for inserting an element of type T in the database.
	 * @param newObject The object to be inserted.
	 */
	public void insert(T newObject) {
		
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement insertStatement = null;
		String insertQuery = createInsertQuery("id", newObject);
		
		try {
			
			insertStatement = connection.prepareStatement(insertQuery);
			insertStatement.executeUpdate();
		} catch(Exception E) {
			
			System.out.println(E.getMessage());
		}
		
	}

	/**
	 * Executes the delete query for the given id.
	 * @param id Represents the id of the element to be deleted.
	 */
	public void delete(int id) {
		
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement deleteStatement = null;
		String deleteQuery = createDeleteQuery("id");
		
		try {
			
			deleteStatement = connection.prepareStatement(deleteQuery);
			deleteStatement.setInt(1, id);
			deleteStatement.executeUpdate();
		} catch(Exception E) {
			
			System.out.println(E.getMessage());
		}
		
	}
	
	/**
	 * Executes the update query on the database. 
	 * @param updatedObject Represents the object with the updated fields. 
	 * @param id Represents the id of the object to be updated.
	 */
	public void update(T updatedObject, int id) {
		
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement updateStatement = null;
		String updateQuery = createUpdateQuery("id", updatedObject);
		
		try {
			
			updateStatement = connection.prepareStatement(updateQuery);
			updateStatement.setInt(1, id);
			updateStatement.executeUpdate();
			
		} catch(Exception E) {
			
			System.out.println(E.getMessage());
		}
	}
	
	/**
	 * Transforms a result set returned by a query in a list of objects of the parameter type.
	 * @param rS Represents the result set returned by the query.
	 * @return Returns the list of objects corresponding to the result set.
	 */
	public List<T> createObjectList(ResultSet rS) {
		
		List<T> listOfObjects = new ArrayList<T>();
		try {

			while(rS.next()) {
				T newObject = type.newInstance();
				for(Field F: type.getDeclaredFields()) {
					Object oneField = rS.getObject(F.getName());
					PropertyDescriptor prDescriptor = new PropertyDescriptor(F.getName().toLowerCase(), type);
					Method wrMethod = prDescriptor.getWriteMethod();
					wrMethod.invoke(newObject, oneField);
				}
				listOfObjects.add(newObject);
			}
		} catch(Exception e) {
			
			System.out.println("HERE" + e.getMessage() +" " +  e.getClass());
		}
		return listOfObjects;
	}
}
