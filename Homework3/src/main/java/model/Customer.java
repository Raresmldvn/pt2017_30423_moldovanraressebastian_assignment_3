package model;

/**
 * Customer represents the class related to the table 'customer' in the database.
 * It is a basic class that implements only constructors and reading/writing methods
 * @author rares
 * @version 1.0
 * @since 2017-04-31
 */

public class Customer {

	private int id;
	private String name;
	private String address;
	private String email;
	private String phone;
	
	/**
	 * Takes the attributes of a real customer and construct an object.
	 * @param cId This corresponds to the id in the database
	 * @param cName This correspond to the real name of the customer
	 * @param cAddress This corresponds to the address of the customer
	 * @param cEmail This corresponds to the email of the customer
	 * @param cPhoneNumber This corresponds to the phone number of the customer
	 */
	public Customer(int cId, String cName, String cAddress, String cEmail, String cPhoneNumber) {
		
		this.id = cId;
		this.name = cName;
		this.address = cAddress;
		this.email = cEmail;
		this.phone = cPhoneNumber;
	}
	
	public Customer() {
		
		this.id = 0;
		this.name = "";
		this.address = "";
		this.email = "";
		this.phone= "";
	}
	
	/**
	 * @return Returns the id of a customer.
	 */
	public int getId() {
		
		return id;
	}
	/**
	 * @return Returns the name of the customer.
	 */
	public String getName() {
		
		return name;
	}
	
	/**
	 * @return Returns the email of the customer.
	 */
	public String getEmail() {
		
		return email;
	}
	
	/**
	 * @return Returns the address of the customer.
	 */
	public String getAddress() {
		
		return address;
	}
	
	/**
	 * @return Returns the phone number of a customer as a string.
	 */
	public String getPhone() {
		
		return phone;
	}
	
	/**
	 * @param newId Sets the id of the customer.
	 */
	public void setId(int newId) {
		
		this.id = newId;
	}
	
	/**
	 * @param newName Sets the name of the customer.
	 */
	public void setName(String newName) {
		
		this.name = newName;
	}
	
	/**
	 * @param newAddress Sets the address of the customer.
	 */
	public void setAddress(String newAddress) {
		
		this.address = newAddress;
	}
	
	/**
	 * @param newPhoneNumber Sets the phone number of the customer.
	 */
	public void setPhone(String newPhoneNumber) {
		
		this.phone = newPhoneNumber;
	}
	
	/**
	 * @param newEmail Sets the email of the customer.
	 */
	public void setEmail(String newEmail) {
		
		this.email  = newEmail;
	}
	
	/**
	 * Converts the customer object into a string.
	 * @return Returns the representation of the customer as a string.
	 */
	public String toString() {
		
		return Integer.toString(id) + "|" + name + "|" + address + "|" + email + "|" + phone;
	}
	
	public boolean isPhonenumber() {
		
		return true;
	}

	
}
