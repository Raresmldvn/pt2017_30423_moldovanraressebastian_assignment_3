package model;

/**
 * OrderDetails represents the class related to the table 'order' in the database which highlights the many-to-many relationship between customer and order.
 * It is a basic class that implements only constructors and reading/writing methods
 * @author rares
 * @version 1.0
 * @since 2017-04-31
 */

public class OrderDetails {

	private int id;
	private int orderid;
	private int productid;
	private int quantity;
	
/**
 * Construct the basic order details explicitly.
 * @param id Represent the identifier for the order detail. 
 * @param oId Represents the order id.
 * @param pId Represents the product id.
 * @param oQuantity
 */
	public OrderDetails(int id, int oId, int pId, int oQuantity) {
		
		this.id = id;
		this.orderid = oId;
		this.productid = pId;
		this.quantity = oQuantity;
		
	}
	
	public OrderDetails() {
		
		this.id = 0;
		this.orderid = 0;
		this.productid = 0;
		this.quantity = 0;
	}
	
	/**
	 * @return Returns the identifier.
	 */
	public int getId() {
		
		return id;
	}
	
	/**
	 * @return Returns the identifier for the order.
	 */
	public int getOrderid() {
		
		return orderid;
	}
	
	/**
	 * @return Return the identifier of the product.
	 */
	public int getProductid() {
		
		return productid;
	}
	
	/**
	 * @return Returns the value in the quantity field.
	 */
	public int getQuantity() {
		
		return quantity;
	}
	
	/**
	 * @param newId setter for the identifier.
	 */
	public void setId(int newId) {
		
		this.id = newId;
	}
	
	/**
	 * @param oId setter for the order identifier.
	 */
	public void setOrderid(int oId) {
		
		this.orderid = oId;
	}
	
	/**
	 * @param pid setter for the product identifier.
	 */
	public void setProductid(int pid) {
		
		this.productid = pid;
	}
	
	/**
	 * @param oQuantity Setter for the quantity field.S
	 */
	public void setQuantity(int oQuantity) {
		
		this.quantity = oQuantity;
	}
	
}
