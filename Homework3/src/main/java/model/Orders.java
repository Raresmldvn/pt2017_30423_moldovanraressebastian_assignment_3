package model;

/**
 * Orders represents the class related to the table 'order' in the database.
 * It is a basic class that implements only constructors and reading/writing methods
 * @author rares
 * @version 1.0
 * @since 2017-04-31
 */

public class Orders {
	
	private int id;
	private int customerid;
	private float total;
	
	/**
	 * 
	 * @param oId Represents the id of the order.
	 * @param oCustomerId Represents the customer id.
	 * @param oTotal Represents the total amount of money for the order.
	 */
	public Orders(int oId, int oCustomerId, float oTotal) {
		
		this.id = oId;
		this.customerid = oCustomerId;
		this.total = oTotal;
	}
	
	public Orders() {
		
		this.id = 0;
		this.customerid = 0;
		this.total = 0;
	}
	
	/**
	 * @return Returns the identifier for the order.
	 */
	public int getId() {
		
		return id;
	}
	
	/**
	 * @return Returns the customer id for the order.
	 */
	public int getCustomerid() {
		
		return customerid;
	}
	
	/**
	 * @return Returns the total amount of money.
	 */
	public float getTotal() {
		
		return total;
	}
	
	/**
	 * @param oId The order id to be set.
	 */
	public void setId(int oId) {
		
		this.id = oId;
	}
	
	/**
	 * @param oCustomerId The customer id to be set.
	 */
	public void setCustomerid(int oCustomerId) {
		
		this.customerid = oCustomerId;
	}
	
	/**
	 * @param oTotal The total id to be set.
	 */
	public void setTotal(float oTotal) {
		
		this.total = oTotal;
	}
}
