package model;

/**
 * Product represents the class related to the table 'product' in the database.
 * It is a basic class that implements only constructors and reading/writing methods
 * @author rares
 * @version 1.0
 * @since 2017-04-31
 */

public class Product {

	private int id;
	private String name;
	private int stock;
	private float price;
	
	public Product() {
		this.id = 0;
		this.name = "";
		this.stock = 0;
		this.price = 0;
	}
	
	/**
	 * The explicit construct for the Product class. 
	 * @param pId This represents the id of the product. 
	 * @param pName This represents the name of the product.
	 * @param pStock This represents the stock in the database of the product.
	 * @param pPrice This represents the price of the product.
	 */
	public Product(int pId, String pName, int pStock, float pPrice) {
		
		this.id = pId;
		this.name = pName;
		this.stock = pStock;
		this.price = pPrice;
	}
	
	/**
	 * @return Returns the id of the product.
	 */
	public int getId() {
		
		return id;
	}
	/**
	 * @return Returns the name of the product.
	 */
	public String getName() {
		
		return name;
	}
	
	/**
	 * @return Returns the stock of the product.
	 */
	public int getStock() {
		
		return stock;
	}
	
	/**
	 * @return Return the price of the product.
	 */
	public float getPrice() {
		
		return price;
	}
	
	/**
	 * @param pId Represents the id to be set.
	 */
	public void setId(int pId) {
		
		this.id = pId;
	}
	
	/**
	 * @param pName Represents the name to be set.
	 */
	public void setName(String pName) {
		
		this.name = pName;
	}
	
	/**
	 * @param pStock Represents the stock to be set.
	 */
	public void setStock(int pStock) {
		
		this.stock = pStock;
	}
	
	/**
	 * @param pPrice Represents the price to be set.
	 */
	public void setPrice(float pPrice) {
		
		this.price = pPrice;
	}
	
	/**
	 * Converts the customer object into a string.
	 * @return Returns the representation of the customer as a string.
	 */
	public String toString() {
		
		return Integer.toString(id) + "|" + name + "|" + Integer.toString(stock) + "|" + Float.toString(price);
	}
}
