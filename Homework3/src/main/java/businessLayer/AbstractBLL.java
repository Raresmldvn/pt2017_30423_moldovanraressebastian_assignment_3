package businessLayer;
import java.lang.reflect.Field;
import java.util.*;


import javax.swing.*;
import javax.swing.table.*;

/**
 * Abstract class that implements a general method for creating a JTable from a list of objects using reflection techniques.
 * @author rares
 * @version 1.0
 * @since 2014-03-15
 * @param <T>
 */
public class AbstractBLL<T> {
	
	/**
	 * Method that creates a table by taking a list of elements of a generic type. It uses reflection techniques for getting the field of the object.
	 * It also uses the values stored in the object to create String lists that will be stored in the table.
	 * @param elements The list of objects.
	 * @return The JTable is returned corresponding to the list of elements.
	 */
	public JTable createTable(List<T> elements) {
		

		JTable finalTable = new JTable();
		
		int rows = elements.size();
		if(rows==0) 
			return new JTable(); //return an empty table for no object
		
		int columns = elements.get(0).getClass().getDeclaredFields().length;
		String columnNames[] = new String[columns]; 
		int i=0;
		for(Field currentField: elements.get(0).getClass().getDeclaredFields()) { //get the field and add them as titles to the column names
			
			currentField.setAccessible(true);
			columnNames[i] = currentField.getName();
			i++;
		}
		Object[][] tableData = new Object[rows][columns];
		try {
		for(i=0; i<rows; i++) {
			
			Object[] oneRow = new String[columns+1]; //create an object containing one row of data
			int j=0;
			for(Field currentField: elements.get(0).getClass().getDeclaredFields()) {
				
				currentField.setAccessible(true); //set the values of the object to accesible
				oneRow[j] = currentField.get(elements.get(i)).toString();
				j++;
			}
			tableData[i] = oneRow; //add the row to the table data
		}
		
		DefaultTableModel model = new DefaultTableModel(tableData, columnNames);
		finalTable = new JTable(model);
	}catch(Exception e) {
		
		System.out.println(e.getClass() + e.toString() + " " + e.getMessage());
	}
		return finalTable;
	}
	
}
