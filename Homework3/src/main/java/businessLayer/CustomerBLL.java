package businessLayer;
import dataAccessLayer.CustomerDAO;
import model.Customer;
import java.io.*;
import javax.swing.*;

/**
 * Class that implements the business logic for the Customer class.
 * It uses the DAO objects to connect to the database and it also validates the user input.
 * @author rares
 * @version 1.0
 * @since 2014-04-15
 */
public class CustomerBLL extends AbstractBLL<Customer>{

	/**
	 * Method that inserts the customer in the database and it also validates the input format.
	 * @param C Represent the customer to be inserted in the database.
	 * @throws IOException
	 */
	public void insertCustomer(Customer C) throws IOException {
		
		CustomerDAO CD = new CustomerDAO();
		if(C.getPhone().matches("[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]")==false) {
			
			throw new IOException("Wrong phone number format!");
		}
		if(C.getEmail().contains("@")==false) {
			
			throw new IOException("Wrong email format!");
		}
		CD.insert(C);
		
	}
	
	/**
	 * Method that selects the objects that need to be deleted by getting the selected rows from the table.
	 * @param table The current table displayed to the user(the table generated in the abstract BLL)
	 */
	public void deleteFromTable(JTable table) {
		
		int[] toDelete = table.getSelectedRows();
		CustomerDAO CD = new CustomerDAO();
		for(int i=0; i<toDelete.length; i++) {
			
			Object o = table.getValueAt(toDelete[i], 0);
			int idToDelete =Integer.parseInt(o.toString());
			CD.delete(idToDelete);
		}
	}
	
	/**
	 * Method that gets the selected rows from the table and their data and updates the necessary rows in the database.
	 * @param table Represents the generated table from the database.
	 */
	public void updateFromTable(JTable table) {
		
		int[] toUpdate = table.getSelectedRows();
		CustomerDAO CD = new CustomerDAO();
		for(int i=0; i<toUpdate.length; i++) {
		
			Object index = table.getValueAt(toUpdate[i], 0);
			int idToUpdate =Integer.parseInt(index.toString());
			String name = table.getValueAt(toUpdate[i], 1).toString();
			String address = table.getValueAt(toUpdate[i], 2).toString();
			String email = table.getValueAt(toUpdate[i], 3).toString();
			String phone = table.getValueAt(toUpdate[i], 4).toString();
			Customer C = new Customer(idToUpdate, name, address, email, phone);
			CD.update(C, idToUpdate);
		}
	}
	
	/**
	 * Gets the selected rows from the database representation table.
	 * @param table Represents the current table representing the database.
	 * @return Returns the first selected customer from the database as a Customer object.
	 */
	public Customer getSelected(JTable table) {
		
		int toReturn = table.getSelectedRow();
		int index = Integer.parseInt(table.getValueAt(toReturn, 0).toString());
		String name = table.getValueAt(toReturn, 1).toString();
		String address = table.getValueAt(toReturn, 2).toString();
		String email = table.getValueAt(toReturn, 3).toString();
		String phone = table.getValueAt(toReturn, 4).toString();
		return new Customer(index, name, address, email, phone);
	}
	
}
