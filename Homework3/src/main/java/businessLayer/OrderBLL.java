package businessLayer;
import model.Orders;
import java.io.*;
import dataAccessLayer.OrderDAO;

/**
 * Class that implements the business logic for the order. It has as main attribute the current order the system is operating on.
 * @author rares
 * @version 1.0
 * @since 2017-04-15
 */
public class OrderBLL {

	private Orders currentOrder;
	
	/**
	 * @return Returns the current order.
	 */
	public Orders getOrder() {
		
		return currentOrder;
	}
	
	/**
	 * Method that inserts into the the database using the DAO object.
	 * @param newOrder
	 * @throws IOException
	 */
	public void insertOrder(Orders newOrder) throws IOException {
		
		new OrderDAO().insert(newOrder);
		currentOrder = newOrder;
	}
	
	/**
	 * Method that updates the orders.
	 * @param newOrder Represents the new order to be inserted in the database.
	 * @param index Represents the index of of the order to be updated.
	 */
	public void updateOrder(Orders newOrder, int index) {
		
		new OrderDAO().update(newOrder, index);
		currentOrder = newOrder;
		
	}
	
	/**
	 * Methods that empties the fields of the current order.
	 */
	public void emptyCurrentOrder() {
		
		this.currentOrder = new Orders();
	}
	
	/**
	 * Drop the current order by deleting it from the database and updating the current order to an empty order.
	 */
	public void drop() {
		
		new OrderDAO().delete(currentOrder.getId());
		this.currentOrder = new Orders();
	}
}
