package businessLayer;
import dataAccessLayer.OrderDetailsDAO;
import model.OrderDetails;
import model.Product;
import dataAccessLayer.ProductDAO;
import java.util.*;

/**
 * Class that implements the business logic for the order details. It has as main attribute the current list of order details the system is operating on.
 * @author rares
 * @version 1.0
 * @since 2017-04-20
 */
public class OrderDetailsBLL {

	List<OrderDetails> orders = new ArrayList<OrderDetails>();
	/**
	 * Method that inserts in the database a new order detail.
	 * @param newOD Represent the order details object to be inserted in the database.
	 */
	public void insert(OrderDetails newOD) {
		
		new OrderDetailsDAO().insert(newOD);
		orders.add(newOD);
	}
	
	/**
	 * @return The current list of order details the system is operating on.
	 */
	public List<OrderDetails> getList() {
		
		return orders;
	}
	
	/**
	 * Update the stocks in the product table accordingly with the order details in the current order.
	 * It uses the ProductDAO class to update the stocks.
	 */
	public void updateStocks() {
		
		ProductDAO pDAO = new ProductDAO();
		for(OrderDetails current:orders) {
			
			int id = current.getProductid();
			Product toUpdate = pDAO.selectById(id);
			pDAO.update(new Product(id, toUpdate.getName(), toUpdate.getStock() - current.getQuantity(), toUpdate.getPrice()), id);
		}
		orders = new ArrayList<OrderDetails>();
	}
	
	
	/**
	 * Deletes the order details from the database and set the current list to null list.
	 */
	public void drop() {
		OrderDetailsDAO odDAO = new OrderDetailsDAO();
		for(OrderDetails current:orders) {
			
			odDAO.delete(current.getId());
		}
		orders = new ArrayList<OrderDetails>();
	}
}
