package businessLayer;
import model.*;
import dataAccessLayer.*;


import java.io.IOException;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

import java.util.*;



import org.apache.pdfbox.pdmodel.PDPageContentStream;

/**
 * Class that implements the capability of creating bills for every finalized order in the system.
 * @author rares
 * @version 1.0
 * @since 2017-04-15
 */
public class BillGenerator {

	/**
	 * Method uses the apache PDF library for creating and completing a PDF with one page containing details about the finalized order.
	 * @param order The order object that characterizes the currently finalizing order.
	 * @param orderD The list of order details that corresponds to the currently finalizing order.
	 */
	public void generatePDFBill(Orders order, List<OrderDetails> orderD)
	{
	   try 
	   {
		  PDDocument document = new PDDocument(); //create the document
	      PDPage bill = new PDPage(); //create the page
	      document.addPage(bill);
	      PDPageContentStream contentStream = new PDPageContentStream(document, bill); //create the content
	      contentStream.beginText();
	      contentStream.setFont(PDType1Font.COURIER_BOLD, 15);
	      contentStream.newLineAtOffset(150, 750);
	      contentStream.showText("BILL NUMBER " + Integer.toString(order.getId())); //write the bill number in the content stream
	      Customer customer = (new CustomerDAO()).selectById(order.getCustomerid());
	      //write the customer details in the content stream
			contentStream.endText();
			contentStream.beginText();
			contentStream.newLineAtOffset(150, 730);
			contentStream.showText("Name: " + customer.getName());
			contentStream.endText();
			contentStream.beginText();
			contentStream.newLineAtOffset(150, 710);
			contentStream.showText("Address: " + customer.getAddress());
			contentStream.endText();
			contentStream.beginText();
			contentStream.newLineAtOffset(150, 690);
			contentStream.showText("Email: " + customer.getEmail());
			contentStream.endText();
			contentStream.beginText();
			contentStream.newLineAtOffset(150, 670);
			contentStream.showText("Phone number:" + customer.getPhone());
			contentStream.endText();
			
			int k = 630; 
			//write the contents of every order detail by obtaining the product name, quantity and price of every product
			for(OrderDetails currentOD: orderD) {
				
				Product currentProduct = (new ProductDAO()).selectById(currentOD.getProductid());
				contentStream.beginText();
				contentStream.newLineAtOffset(150, k);
				contentStream.showText(currentProduct.getName() + " : " + currentOD.getQuantity() + " x " + currentProduct.getPrice());
				contentStream.endText();
				k = k - 20;;
			}
			contentStream.beginText();
			contentStream.newLineAtOffset(150, k);
			contentStream.showText("TOTAL : " + order.getTotal()); //output the total price that the customer needs to pay
			contentStream.endText();
			
			contentStream.close();  
	      	document.save("BillNo" + Integer.toString(order.getId()) + ".pdf"); //save the document
	   }
	   catch (Exception ioEx)
	   {
	      System.out.println("Exception while trying to create blank document - ");
	   }
	}
	
	public void generateBill(Orders order, List<OrderDetails> orderD) throws IOException {
		
		int orderNo = order.getId();
		BufferedWriter output = null;
		File file = new File("BillNo" + Integer.toString(orderNo) +".txt" );
		
		output = new BufferedWriter(new FileWriter(file));
		output.write("BILL NUMBER " + Integer.toString(orderNo));
		output.newLine();
		
		//get Customer details
		Customer customer = (new CustomerDAO()).selectById(order.getCustomerid());
		System.out.println(customer.toString());
		output.write("Name: " + customer.getName());
		output.newLine();
		output.write("Address: " + customer.getAddress());
		output.newLine();
		output.write("Email: " + customer.getEmail());
		output.newLine();
		output.write("Phone number:" + customer.getPhone());
		output.newLine();
		output.newLine();
		for(OrderDetails currentOD: orderD) {
			
			Product currentProduct = (new ProductDAO()).selectById(currentOD.getProductid());
			output.write(currentProduct.getName() + "....................." + currentOD.getQuantity() + " x " + currentProduct.getPrice());
			output.newLine();
		}
		output.write("TOTAL......................." + order.getTotal());
		output.close();
		
	}
}
