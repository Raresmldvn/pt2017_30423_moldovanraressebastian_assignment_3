package businessLayer;
import java.io.IOException;

import javax.swing.JTable;

import dataAccessLayer.CustomerDAO;
import model.Customer;
import model.Product;
import dataAccessLayer.*;

public class ProductBLL extends AbstractBLL<Product> {
	
/**
 * Insert a product in the table
 * @param P Represent the product to be inserted in the database.
 * @throws IOException
 */
public void insertProduct(Product P) throws IOException {
		
		ProductDAO PD = new ProductDAO();
		PD.insert(P);
		
	}


/**
 * Select from the JTable the cells that need to be deleted and delete from the database.
 * @param table Represents the current representation table of the database.
 */
public void deleteFromTable(JTable table) {
		
		int[] toDelete = table.getSelectedRows();
		ProductDAO PD = new ProductDAO();
		for(int i=0; i<toDelete.length; i++) {
			
			Object o = table.getValueAt(toDelete[i], 0);
			int idToDelete =Integer.parseInt(o.toString());
			PD.delete(idToDelete);
		}
	}

/**
 * Method that obtains the selected rows from the product table and updates them accordingly in an "Excel" style operation.
 * @param table The current representation of the database as a table.
 */
public void updateFromTable(JTable table) {
	
	int[] toUpdate = table.getSelectedRows();
	ProductDAO PD = new ProductDAO();
	for(int i=0; i<toUpdate.length; i++) {
	
		Object index = table.getValueAt(toUpdate[i], 0);
		int idToUpdate =Integer.parseInt(index.toString());
		String name = table.getValueAt(toUpdate[i], 1).toString();
		int stock = Integer.parseInt(table.getValueAt(toUpdate[i], 2).toString());
		float price = Float.parseFloat(table.getValueAt(toUpdate[i], 3).toString());
		Product P = new Product(idToUpdate, name, stock, price);
		PD.update(P, idToUpdate);
	}
}

/**
 * Method that gets the selected rows from the table.
 * @param table The current representation of the product table in the database.
 * @return The selected row as a Product Object.
 */

public Product getSelected(JTable table) {
	
	int toReturn = table.getSelectedRow();
	int id = Integer.parseInt(table.getValueAt(toReturn, 0).toString());
	String name = table.getValueAt(toReturn, 1).toString();
	int stock = Integer.parseInt(table.getValueAt(toReturn, 2).toString());
	float price = Float.parseFloat(table.getValueAt(toReturn, 3).toString());
	return new Product(id, name, stock, price);
}

}
